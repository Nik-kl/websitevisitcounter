package com.nik.initServlet;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.nik.config.SpringWebConfig;

/**
 * No more need for web.xml file 
 * Base class for WebApplicationInitializer implementations that register a DispatcherServlet configured with annotated 
 * classes, e.g. Spring's @Configuration classes.
 * */

public class WebConfigInitializer extends
AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { SpringWebConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}
}
