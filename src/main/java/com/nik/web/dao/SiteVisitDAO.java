package com.nik.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.nik.web.entity.WebSiteVisit;
import com.nik.web.model.SearchCriteria;
import com.nik.web.model.SiteCounter;

/**
 * This class contains all DB related functions on table T_WEBSITE_VISIT (or Entity WebSiteVisit) 
 * */

@Repository("siteVisitDAO")
public class SiteVisitDAO extends AbstractDao {
	
	/**
	 * Fetch records from  WebSiteVisit based on entered input
	 * 
	 * If Start date and end date is not specified then fetch top 5 records from all data present in that table
	 * */
	@SuppressWarnings("unchecked")
	public List<SiteCounter> getTopVisitedSites(SearchCriteria search) {
		Criteria criteria = getSession().createCriteria(WebSiteVisit.class);
		criteria.setMaxResults(search.getMaxRecords());
		
		criteria.setProjection(
				Projections.projectionList()
                .add(Projections.groupProperty("url"), "siteURL")
                .add(Projections.sum("visitCount"), "noOfHits"))
				.setResultTransformer(Transformers.aliasToBean(SiteCounter.class));
		
		if (isSearchCriteriaValid(search)) {
			criteria.add(Restrictions.between("dateRecorded", search.getStartDate(), search.getEndDate()));
		} else {
			// date range is blank. Get top 5 website based on all records
			// This block is intentionally left blank
			// Criteria is created above IF block so if search criteria is not valid it will fetch top 5 records
			// from all data present in that table
		}
		
		if(search.getOrderBy() == 1) {
			criteria.addOrder(Order.desc("noOfHits"));	
		} else {
			criteria.addOrder(Order.asc("noOfHits"));
		}
		
		return (List<SiteCounter>) criteria.list();
	}

	private boolean isSearchCriteriaValid(SearchCriteria search) {
		return !(search == null || ((StringUtils.isEmpty(search.getStartDate())) && (StringUtils.isEmpty(search.getEndDate()))));
	}
}
