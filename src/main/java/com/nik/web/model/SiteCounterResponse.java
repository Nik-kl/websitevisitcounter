package com.nik.web.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.nik.web.jsonView.Views;

/*
 * REST service returns response in JSON format
 * */
public class SiteCounterResponse {

	@JsonView(Views.Counter.class)
	private List<SiteCounter> result;
	
	@JsonView(Views.Counter.class)
	private boolean resultFound;

	public List<SiteCounter> getResult() {
		return result;
	}
	public void setResult(List<SiteCounter> result) {
		this.result = result;
	}
	
	public boolean isResultFound() {
		return resultFound;
	}
	public void setResultFound(boolean resultFound) {
		this.resultFound = resultFound;
	}
	
	@Override
	public String toString() {
		return "SiteCounterResponse [result=" + result + ", resultFound=" + resultFound + "]";
	}
}
