package com.nik.web.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.nik.web.jsonView.Views;

/*
 * REST service returns response in JSON format
 * */
public class SiteCounter {

	@JsonView(Views.Counter.class)
	private String siteURL;
	
	@JsonView(Views.Counter.class)
	private long noOfHits;
	
	public SiteCounter() {
		super();
	}

	public SiteCounter(String siteURL, long noOfHits) {
		super();
		this.siteURL = siteURL;
		this.noOfHits = noOfHits;
	}

	public String getSiteURL() {
		return siteURL;
	}
	public void setSiteURL(String siteURL) {
		this.siteURL = siteURL;
	}

	public long getNoOfHits() {
		return noOfHits;
	}
	public void setNoOfHits(long noOfHits) {
		this.noOfHits = noOfHits;
	}

	@Override
	public String toString() {
		return "SiteCounter [siteURL=" + siteURL + ", noOfHits=" + noOfHits + "]";
	}
}
