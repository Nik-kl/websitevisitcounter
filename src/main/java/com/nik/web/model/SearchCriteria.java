package com.nik.web.model;

import java.util.Date;

/**
 * SearchCriteria class maps data passed to REST request body
 * SearchCriteria object is then passed to DAO for fetching data from DB
 * */
public class SearchCriteria {

	private Date startDate;
	private Date endDate;
	private int maxRecords = 5; // Maps to dropdown on JSP. By default it set max returned record by query to 5
	private int orderBy = 1; // 1 - desc, 0 - asc
	
	public SearchCriteria() {
		super();
	}

	public SearchCriteria(Date startDate, Date endDate, int maxRecords, int orderBy) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.maxRecords = maxRecords;
		this.orderBy = orderBy;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getMaxRecords() {
		return maxRecords;
	}
	public void setMaxRecords(int maxRecords) {
		this.maxRecords = maxRecords;
	}
	
	public int getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}

	@Override
	public String toString() {
		return "SearchCriteria [startDate=" + startDate + ", endDate=" + endDate + ", maxRecords=" + maxRecords
				+ ", orderBy=" + orderBy + "]";
	}
}
