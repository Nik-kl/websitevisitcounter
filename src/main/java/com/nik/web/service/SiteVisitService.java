package com.nik.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nik.web.dao.SiteVisitDAO;
import com.nik.web.model.SearchCriteria;
import com.nik.web.model.SiteCounter;

/**
 * Spring service layer
 * 
 * Currently Transaction is readOnly as there is only SELECT operation 
 * */

@Service("siteVisitService")
@Transactional(readOnly=true)
public class SiteVisitService {
	@Autowired
	private SiteVisitDAO siteVisitDAO;

	public List<SiteCounter> getTopVisitedSites(SearchCriteria search) {
		return siteVisitDAO.getTopVisitedSites(search);
	}
}
