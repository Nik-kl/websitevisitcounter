package com.nik.web.jsonView;

/**
 * Not used to filter response in this application
 * 
 * It can sometimes be useful to filter contextually objects serialized to the HTTP response body. 
 * In order to provide such capabilities, Spring MVC now has builtin support for 
 * Jackson’s Serialization Views (as of Spring Framework 4.2, 
 * JSON Views are supported on @MessageMapping handler methods as well).
 * 
 * */

public class Views {
	public static class Counter {}
}
