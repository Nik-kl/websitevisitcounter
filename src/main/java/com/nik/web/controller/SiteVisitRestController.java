package com.nik.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.nik.web.jsonView.Views;
import com.nik.web.model.SearchCriteria;
import com.nik.web.model.SiteCounter;
import com.nik.web.model.SiteCounterResponse;
import com.nik.web.service.SiteVisitService;

/**
 * REST Service
 * 
 * User click generate Report button on JSP. Then ajax call is made to this service which returns data that JSP renders
 * 
 * This controller is implemented as REST because:
 * 		* Any application (web, desktop), running on any device (e.g. IOS, android) can consume it
 * 		*  Consumer is not restricted to SiteVisitReport only, service can be exposed to outside world
 * */

@RestController
public class SiteVisitRestController {
	
	@Autowired
	private SiteVisitService siteVisitService;
	
	@JsonView(Views.Counter.class)
	@RequestMapping(value = "/topFive")
	public SiteCounterResponse getTopFiveSites(@RequestBody SearchCriteria search) {
		SiteCounterResponse response = new SiteCounterResponse();
		List<SiteCounter> result = siteVisitService.getTopVisitedSites(search);
		
		if(result.isEmpty()) {
			response.setResultFound(false);
		} else {
			response.setResultFound(true);
			response.setResult(result);
		}
		
		return response;
	}
}