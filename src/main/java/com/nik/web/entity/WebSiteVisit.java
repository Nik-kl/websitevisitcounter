package com.nik.web.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_WEBSITE_VISIT")
public class WebSiteVisit implements Serializable {

	private static final long serialVersionUID = 8497714281616294716L;
	
	public WebSiteVisit() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name="DATE",nullable=false)
	private Date dateRecorded;
	
	@Column(name="URL",nullable=false)
	private String url;
	
	@Column(name="VISIT_COUNT",nullable=false)
	private long visitCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateRecorded() {
		return dateRecorded;
	}

	public void setDateRecorded(Date dateRecorded) {
		this.dateRecorded = dateRecorded;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getCount() {
		return visitCount;
	}

	public void setCount(long visitCount) {
		this.visitCount = visitCount;
	}
}
