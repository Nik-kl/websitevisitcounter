/**
 * Used in welcome.jsp
 */

var app = angular.module('myApp', ['ngMaterial','ngMessages']);
	app.controller('myCtrl', function($scope,$http) {
	$scope.showData = false;
	$scope.showError = false;
	
	$scope.startDate = new Date(2013, 01, 01);
	$scope.endDate = new Date();
	
	// Initialize Dropdown
	$scope.topOptions = ["5", "10", "20", "50"];
	$scope.maxRecords = "5";
	
	$scope.sortOrder = [
	               {order : "Highest Hit First", orderVal : "1"},
	               {order : "Lowest Hit First", orderVal : "0"}
	           ];
	$scope.selSortOrder = "1";
	
	$scope.generateReport = function() {
		$scope.showError = false;
		var req;
		var result = false;
		
		if(($scope.startDate == null || $scope.startDate == '') && ($scope.endDate == null || $scope.endDate == '')) {
			result = true;
		} else if(($scope.startDate == null || $scope.startDate == '') || ($scope.endDate == null || $scope.endDate == '')) {
			result = false;
			$scope.showError = true;
	    	$scope.errMsg = 'Both start date and end date should be entered';
		} else if((new Date($scope.endDate.getFullYear(), $scope.endDate.getMonth(), $scope.endDate.getDate()) - new Date($scope.startDate.getFullYear(), $scope.startDate.getMonth(), $scope.startDate.getDate())) > 0) {
	    	result = true;
	    } else {
	    	$scope.showError = true;
	    	$scope.errMsg = 'Start date should be less than end date';
	    }
		
		if(result) {
			req = {
	        		 method: 'POST',
	        		 url: 'topFive',
	        		 headers: {
	        		   'Content-Type': 'application/json'
	        		 },
	        		 data: { 
	        			 "startDate": ($scope.startDate.getFullYear() +'-' +$scope.startDate.getMonth() +'-' +$scope.startDate.getDate()),
	 					 "endDate": ($scope.endDate.getFullYear() +'-' +$scope.endDate.getMonth() +'-' +$scope.endDate.getDate()),
	 					 "maxRecords": $scope.maxRecords,
	 					 "orderBy": $scope.selSortOrder
	        		 }
	        		}

        		$http(req).success(function(response){
        			if(response.resultFound) {
        				$scope.showData = true;
        				$scope.visitCounts = response.result;
        			} else {
        				$scope.showData = false;
        			}
        			
        		}).error(function(response){
        			$scope.showData = false;
        		});					
		} else {
			$scope.showData = false;
		}
    }
});

// Date format
app.config(function($mdDateLocaleProvider) {
  $mdDateLocaleProvider.formatDate = function(date) {
	    return moment(date).format('YYYY-MM-DD');
	  };
});