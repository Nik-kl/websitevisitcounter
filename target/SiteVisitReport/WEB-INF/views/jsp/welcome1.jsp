<%@page session="false"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Website visit counter</title>

<c:url var="home" value="/" scope="request" />

<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />

<spring:url value="/resources/js/jquery.1.10.2.min.js" var="jqueryJs" />
<spring:url value="/resources/js/visits.js" var="visitsJs" />
<script src="${jqueryJs}"></script>
<script src="${visitsJs}"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</head>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Website visit counter</a>
			</div>
		</div>
	</nav>

	<div class="container" style="min-height: 500px">  
		<div class="starter-template">
			<h2>Top 5 visited websites</h2>
			<br>
		
			<form class="form-horizontal" id="search-form">
				<table class="table table-striped">
				    <tbody>
				      <tr>
				        <td class="pull-right">Start Date</td>
				        <td><input type=text class="form-control" id="start"></td>
				        <td class="pull-right">End Date</td>
				        <td><input type=text class="form-control" id="end"></td>
				        <td class="pull-right">Show Top</td>
				        <td>
				        	<select id="maxRecords">
				        		<option value="5">5</option>
				        		<option value="10">10</option>
				        		<option value="20">20</option>
				        		<option value="50">50</option>
				        	</select>
				        </td>
				        <td class="pull-right">Order</td>
				        <td>
				        	<select id="sortOrder">
				        		<option value="1">Highest Hit First</option>
				        		<option value="0">Lowest Hit First</option>
				        	</select>
				        </td>
				        <td><button id="btn-search" class="btn btn-primary">Report</button>
						</td>
				      </tr>
				    </tbody>
			   </table>
			</form>
		</div>
		<div class="alert alert-danger" id="errorDiv">&nbsp;</div>
		
		<div id="feedback">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Website URL</th>
						<th>Visit Count</th>
					</tr>
				</thead>
			  <tbody id="displayResult">&nbsp;</tbody>
			</table>
		</div>
	</div>
	
	<div class="container">
		<footer>
			<p>
				&nbsp;
			</p>
		</footer>
	</div>
</body>
</html>