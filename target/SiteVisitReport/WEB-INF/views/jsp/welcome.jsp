<%@page session="false"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Website visit counter</title>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">


<spring:url value="/resources/js/angular.min.js" var="angularJs" />
<spring:url value="/resources/js/angular-animate.min.js" var="animateJs" />
<spring:url value="/resources/js/angular-aria.min.js" var="ariaJs" />
<spring:url value="/resources/js/angular-messages.min.js" var="messagesJs" />
<spring:url value="/resources/js/angular-material.min.js" var="materialJs" />
<spring:url value="/resources/js/moment.js" var="momentJs" />

<spring:url value="/resources/js/visits.js" var="visitsJs" />

<script src="${angularJs}"></script>
<script src="${animateJs}"></script>
<script src="${ariaJs}"></script>
<script src="${messagesJs}"></script>
<script src="${materialJs}"></script>
<script src="${momentJs}"></script>
<script src="${visitsJs}"></script>

<style>
	table, th , td  {
	  border: 1px solid grey;
	  border-collapse: collapse;
	  padding: 5px;
	}
	table tr:nth-child(odd) {
	  background-color: #f1f1f1;
	}
	table tr:nth-child(even) {
	  background-color: #ffffff;
	}
	
.datepickerdemo md-content {
    padding-bottom: 200px; 
 }
</style>
</head>

<body style="padding-left: 20px;">

	<p><b>Website visit counter</b></p>

	<div data-ng-app="myApp" data-ng-controller="myCtrl">
		<form class="form-horizontal" id="search-form">
			<table class="table table-striped">
			    <tbody>
			      <tr>
			        <td class="pull-right">Start Date</td>
			        <td>
		        		<md-datepicker 
		                  name="dateField" 
		                  ng-model="startDate" 
		                  md-placeholder="Enter date"
		                  ></md-datepicker>
			        </td>
			        <td class="pull-right">End Date</td>
			        <td>
			        	<md-datepicker 
		                  name="dateField" 
		                  ng-model="endDate"
		                  md-placeholder="Enter date"
		                  ></md-datepicker>
			        </td>
			        <td class="pull-right">Show Top</td>
			        <td>
			        	<select data-ng-model="maxRecords" >
							<option data-ng-repeat="x in topOptions">{{x}}</option>
						</select>
			        </td>
			        <td class="pull-right">Order</td>
			        <td>
			        	<select data-ng-model="selSortOrder">
			        		<option data-ng-repeat="x in sortOrder" value="{{x.orderVal}}">{{x.order}}</option>
			        	</select>
			        </td>
			        <td><button id="btn-search" class="btn btn-primary" data-ng-click="generateReport()">Report</button>
					</td>
			      </tr>
			    </tbody>
		   </table>
		</form>
		
		<br/>
		<div style="color:maroon" role="alert" data-ng-show="showError">&nbsp;{{errMsg}}</div>
		
		<br/>
		<div data-ng-show="showData">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Website URL</th>
						<th>Visit Count</th>
					</tr>
				</thead>
			  <tbody>
			  	<tr data-ng-repeat = "visitCo in visitCounts">
			  		<td>{{ visitCo.siteURL }}</td>
			  		<td>{{ visitCo.noOfHits | number}}</td>
			  	</tr>
			  </tbody>
			</table>
		</div>
	</div>
</body>
</html>